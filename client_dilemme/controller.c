#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/socket.h>

#include <stdbool.h>
#include "controller.h"
#include "../common/communication.h"
#include "../common/variables.h"

/**
 * @brief Action from the client
 * @param choice
 * @param sockfd
 * @param msg
 * @return 
 **/
void executeAction(int choice, int sockfd, Message msg)
{
    msg.action = choice;
    char *message = serialize(msg);
    printf("\n ***Message sent***");
    logClient(msg);
    send_message(message, sockfd);
}

/**
 * @brief Send message to the server
 * @param message
 * @param sockfd
 * @return 
 **/
void send_message(char *message, int sockfd)
{
    write(sockfd, message, strlen(message));
}

/**
 * @brief Get matching message for the gui
 * @param label
 * @param cli_protocol
 * @return The message
 **/
char *get_message(int label, Message msg)
{
    char *message = malloc(100);
    switch (label)
    {
    case WAITING:
        message = "Waiting for another player";
        break;
    case GAMEWAITINGRESULTPLAYER:
        message = "Waiting for results";
        break;
    case BOTHBETRAY:
        message = "You both betrayed";
        break;
    case GAMESTART:
        message = "Your turn to play";
        break;
    case GAME_CLIENT_WAITING_SERVER:
        message = (label == STATUSMESSAGE) ? "Waiting for the server" : "";
        break;
    case BOTHCOLLAB:
        message = "You both collaborated";
        break;
    case GAMEEND:
        message = "Game over";
        break;
    case DRAW:
        message = "Draw";
        break;

    case P1WIN:
        if (msg.status == GAMEEND)
        {
            message = (msg.player_id % 2 == 0) ? "You lost the game" : "You won the game";
        }
        else
        {
            message = (msg.player_id % 2 == 0) ? "You have been betrayed" : "You betrayed";
        }
        break;
    case P2WIN:
        if (msg.status == GAMEEND)
        {
            message = (msg.player_id % 2 == 0) ? "You won the game" : "You lost the game";
        }
        else
        {
            message = (msg.player_id % 2 == 0) ? "You betrayed" : "You have been betrayed";
        }
        break;

    default:
        break;
    }
    return message;
}

/**
 * @brief display message when client starts
 **/
void displayInfoClient()
{
    printf("*****************Open client*****************\n");
}

/**
 * @brief log for the message
 * @param msg
 * @return
 **/
void logClient(Message msg)
{
    printf("\n - Protocol => Gameid : %d ; Status : %d ; IdPlayer : %d ; Action : %d ; Round : %d ; Montant : %d \n",
           msg.game_id, msg.status, msg.player_id,
           msg.action, msg.round, msg.montant);
}