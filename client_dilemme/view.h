
#ifndef VIEW_H
#define VIEW_H

#include "../common/communication.h"
#include <pthread.h>

void on_window_main_destroy();
void timer_handler();
void on_clicked_start_stop();
void on_button_trahir();
void on_button_cooperate();
void on_cancel();
void update_view_data(Message msg);
void on_button_click();
void set_message(int label);
void set_amount();
void set_status(int status);
void set_round();
void initialize_window(int argc, char** argv, int sockfdd, pthread_t thread);
#endif