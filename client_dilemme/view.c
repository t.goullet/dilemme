#include <gtk/gtk.h>
#include <time.h>
#include <string.h>
#include "controller.h"
#include "socket.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "../common/variables.h"

GtkBuilder *builder = NULL;
int sockfd;
Message message_client;

/**
 * @brief Set the client status
 * @param status
 **/
void set_status(int status)
{
    message_client.status = status;
}

/**
 * @brief Close the windows and disconnect from the server
 **/
void on_window_main_destroy()
{
    set_status(DISCONNECTED);
    executeAction(0, sockfd, message_client);
    gtk_main_quit();
}

/**
 * @brief update labels on the interface
 * @param label
 **/
void set_message(int label)
{
    char *message = malloc(128);
    GtkLabel *gtk_label;
    if (label == STATUSMESSAGE)
    {
        label = message_client.status;
        gtk_label = GTK_LABEL(gtk_builder_get_object(builder, "status_label"));
    }
    else if (label == ACTIONMESSAGE)
    {
        label = message_client.action;
        gtk_label = GTK_LABEL(gtk_builder_get_object(builder, "action_label"));
    }
    message = get_message(label, message_client);
    gtk_label_set_text(gtk_label, message);
}

/**
 * @brief Update the number of round
 **/
void set_round()
{
    char labelContent[64];
    sprintf(labelContent, "Round : %d", message_client.round);
    GtkLabel *label_round = GTK_LABEL(gtk_builder_get_object(builder, "round_label"));
    gchar *display;
    display = g_strdup_printf("%d", labelContent);
    gtk_label_set_text(label_round, display);
}

/**
 * @brief Update the amount of the player
 **/
void set_amount()
{
    char labelContent[16];
    GtkLabel *labelAMount = GTK_LABEL(gtk_builder_get_object(builder, "amount"));
    sprintf(labelContent, "%d€", message_client.montant);
    gtk_label_set_text(labelAMount, labelContent);
}

/**
 * @brief Update the entire view
 **/
void refresh_view()
{
    GtkButton *btnBetray = GTK_BUTTON(gtk_builder_get_object(builder, "trahir"));

    GtkButton *btnCooperate = GTK_BUTTON(gtk_builder_get_object(builder, "collaborer"));
    if (message_client.status == GAMEEND)
    {
        gtk_widget_hide(GTK_WIDGET(btnCooperate));
        gtk_widget_hide(GTK_WIDGET(btnBetray));
    }

    set_round();
    set_amount();
    set_message(STATUSMESSAGE);
    set_message(ACTIONMESSAGE);
}

/**
 * @brief Function to update data of the interface
 * @param input_protocol
 **/
void update_view_data(Message msg)
{
    message_client.game_id = msg.game_id;
    message_client.round = msg.round;
    message_client.player_id = msg.player_id;
    message_client.action = msg.action;
    message_client.status = msg.status;
    message_client.montant = msg.montant;
    refresh_view();
}

/**
 * @brief Function for the selection of betray choice
 **/
void on_button_betray()
{
    if (message_client.status == GAMESTART)
    {
        set_status(GAMEWAITINGRESULTPLAYER);
        executeAction(BETRAY, sockfd, message_client);
        set_message(STATUSMESSAGE);
    }
}

/**
 * @brief Action when player decides to cooperate
 **/
void on_button_cooperate()
{
    if (message_client.status == GAMESTART)
    {
        set_status(GAMEWAITINGRESULTPLAYER);
        executeAction(COLLABORATE, sockfd, message_client);
        set_message(STATUSMESSAGE);
    }
}

/**
 * @brief Leave the game and disconnect
 **/
void on_cancel()
{
    GtkWidget *message_dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
                                                       GTK_MESSAGE_WARNING,
                                                       GTK_BUTTONS_OK_CANCEL,
                                                       "This action will cause the universe to stop existing.");
    unsigned int pressed = gtk_dialog_run(GTK_DIALOG(message_dialog));
    if (pressed == GTK_RESPONSE_OK)
    {
        printf("\nDisconnect.. \n");
        set_status(DISCONNECTED);
        executeAction(0, sockfd, message_client);
        gtk_widget_destroy(message_dialog);
        gtk_main_quit();
    }
    else
    {
        gtk_widget_destroy(message_dialog);
    }
}

/**
 * @brief Function to initialize the client interface with reading thread
 * @param argc
 * @param argv
 * @param sockfdd
 * @param thread
 **/
void initialize_window(int argc, char **argv, int sockfdd, pthread_t thread)
{
    GtkWidget *win;
    sockfd = sockfdd;

    gtk_init(&argc, &argv);
    builder = gtk_builder_new_from_file("gui/Interface.glade");
    win = GTK_WIDGET(gtk_builder_get_object(builder, "app_win"));
    g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme", TRUE, NULL);
    gtk_builder_connect_signals(builder, NULL);
    gtk_widget_show(win);

    pthread_create(&thread, 0, thread_process, &sockfd);
    pthread_detach(thread);

    gtk_main();
}
