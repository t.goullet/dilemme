#ifndef UTILS_H
#define UTILS_H


#define BUFFERSIZE 2048


#include "../common/communication.h"

void executeAction(int choice, int sockfd, Message msg);
void send_message(char *message, int sockfd);
char *get_message(int label, Message msg);
void displayInfoClient();
void logClient(Message msg);

#endif /* CLIENTCXNMANAGER_H */

