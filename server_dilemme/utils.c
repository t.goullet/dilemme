#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "utils.h"
#include "../common/variables.h"
#include "../common/communication.h"
#include "game.h"
#include "server.h"

/**
 * @brief Take the code in paramater to define the message to show
 * @param response_player
 * @return the message to show
 **/
char *codeToDisplay(int response_player)
{
    char *result = malloc(64);
    switch (response_player)
    {
    case P1WIN:
        result = "PLAYER 1 WIN";
        break;
    case P2WIN:
        result = "PLAYER 2 WIN";
        break;
    case COLLABORATE:
        result = "COLLABORATE";
        break;
    case BETRAY:
        result = "BETRAY";
        break;
    case BOTHCOLLAB:
        result = "BOTH COLLAB";
        break;
    case BOTHBETRAY:
        result = "BOTH BETRAY";
        break;

    default:
        break;
    }
    return result;
}


/**
 * @brief Send message
 * @param input_protocol
 * @param connection
 * @return
 **/
void sendMessage(Message input_protocol, connection_t *connection)
{
    char *outputCLient = serialize(input_protocol);
    write(connection->sockfd, outputCLient, strlen(outputCLient));
}
