#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "game.h"
#include "games.h"
#include "../common/variables.h"
#include "../common/communication.h"
#include "utils.h"

Game_settings game_setting;

int roundParty;

/**
 * @brief Init the game settings with the global settings
 * @param Settings
 **/
void initializeGameSettings(Game_settings settings)
{
    game_setting = settings;
}

/**
 * @brief Create a new game
 * @param id id
 * @return game created
 **/
Game *newGame(int id)
{
    Game *new_game = malloc(sizeof(Game));
    new_game->id = id;
    new_game->maxRound = game_setting.rounds;
    new_game->result_round = 0;
    new_game->private = 0;
    new_game->round = 0;
    new_game->p1Response = JOIN;
    new_game->p1Amount = 0;
    new_game->p2Response = WAITINGNEWPLAYER;
    new_game->p2Amount = 0;
    new_game->status = WAITING;

    return new_game;
}
/**
 * @brief Add new game to the games array
 * @param input_protocol
 * @param connection
 * @param game
 * @return game initialized
 **/
Game *addNewGame(connection_t *connection, Game *game)
{
    srand(time(0));
    game = newGame(rand() % 1000);
    game->player_1 = connection;
    addGame(*game);
    return game;
}

/**
 * @brief Send message to both players
 * @param input_protocol
 * @param connection
 * @param game
 **/
void startGame(Message msg, connection_t *connection, Game *game)
{
    // Host
    msg.player_id = game->player_1->index;
    sendMessage(msg, game->player_1);
    // Second
    sendMessage(msg, connection);
}

/**
 * @brief match a player with an already existing game
 * @param connection
 * @param game
 * @return the game the player has been put in
 **/
Game *join_game(connection_t *connection, Game *game)
{
    game->player_2 = connection;
    game->p2Amount = READY;
    game->p1Response = READY;
    game->private = 1;
    return game;
}

/**
 * @brief Find round result
 * @param game
 * @return game with points updated
 **/
Game *find_result(Game *game)
{
    if (game->p1Response == game->p2Response &&
        game->p1Response == COLLABORATE)
    {
        game->result_round = BOTHCOLLAB;
    }
    else if (game->p1Response == game->p2Response &&
             game->p1Response == BETRAY)
    {
        game->result_round = BOTHBETRAY;
        game->p1Amount = game->p1Amount - game_setting.bettingAmount;
        game->p2Amount = game->p2Amount - game_setting.bettingAmount;
    }
    else if (game->p1Response == BETRAY &&
             game->p2Response == COLLABORATE)
    {
        game->result_round = P1WIN;
        game->p1Amount = game->p1Amount + game_setting.bettingAmount;
        game->p2Amount = game->p2Amount - game_setting.bettingAmount;
    }
    else
    {
        game->result_round = P2WIN;
        game->p1Amount = game->p1Amount - game_setting.bettingAmount;
        game->p2Amount = game->p2Amount + game_setting.bettingAmount;
    }
    return game;
}

/**
 * @brief Find game result
 * @param game
 * @return winner
 **/
int findWinner(Game *game)
{
    int ret = DRAW;
    if (game->p1Amount > game->p2Amount)
    {
        ret = P2WIN;
    }
    else if (game->p1Amount < game->p2Amount)
    {
        ret = P1WIN;
    }
    return DRAW;
}
