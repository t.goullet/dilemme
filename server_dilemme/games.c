#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "games.h"


Game_array games;


/**
 * @brief Search game with the id passed in parameter
 * @param id
 * @return Return game found or NULL
 **/
Game* searchById(int id){
    for(int i = 0; i<= games.index; i++){
        if(games.data[i].id == id){
            return &games.data[i];
        }
    }
    return NULL;
}

/**
 * @brief search game valid in the game_array
 * @return Game if found a game valid
 **/
Game* searchValidGame(){

    for(int i = 0; i<= games.index; i++){
        if(games.data[i].p2Response == WAITINGNEWPLAYER){
            return &games.data[i];
        }
    }
    return NULL;
}

/**
 * @brief init games array
 **/
void initGames() {
    games.index = 0;
}

/**
 * @brief Add game to games array
 * @param game
 **/
void addGame(Game game) {
    if (games.index < MAX_GAME) {
        games.data[games.index + 1] = game;
        games.index ++;      
    }
}

/**
 * @brief remove the game at the index given in paramater
 * @param index
 * @return true if succeeded or false
 **/
bool removeGame(int index){
    if(index <= games.index){
        for(int i = index; i <= (games.index -1); i++){
           games.data[i] = games.data[i + 1];
        }
        games.index--;
        return true;
    }
    return false;
}