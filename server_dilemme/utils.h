

#ifndef UTILS_H
#define UTILS_H

#include "../common/communication.h"
#include "server.h"

char *codeToDisplay(int response_player);
void sendMessage(Message outputProtocol, connection_t* connection);

#endif 
