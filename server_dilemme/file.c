#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "game.h"
#include "utils.h"
#include <time.h>

char path[2048];
/**
 * @brief Write round result in the file
 * @param game
 * @return
 **/
void addResult(Game* game){
    FILE* file = NULL;
    file = fopen(path, "a");
    fprintf(file, "%d;%d;%d;%s;%f;%f\n",
        game->id,
        game->round, 
        codeToDisplay(game->p1Response), 
        game->p1Amount  
        ,codeToDisplay(game->p2Response), 
        game->p2Amount);
    fclose(file);
}

/**
 * @brief create csv file to store result
 * @return
 **/
void createFile(char *filename){
    FILE* file = NULL;
    
    strcat(path,"../");
    strcat(path, filename);

    file = fopen(path, "w");
    if(file != NULL) {
        fprintf(file, "id;round;p1Action;p1Amount;p2Action;p2Amount;\n");
        fclose(file);
    }
}