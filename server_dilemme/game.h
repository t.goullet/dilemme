
#ifndef GAME_H
#define GAME_H

#include "server.h"
#include "../common/communication.h"

// Structure of one game
typedef struct
{
    int id;
    int status;
    connection_t *player_1;
    int p1Response;
    int p1Amount;
    connection_t *player_2;
    int p2Response;
    int p2Amount;
    int private;
    int result_round;
    int maxRound;
    int round;

} Game;

// Structure that contains the parameters of the configuration files part
typedef struct
{
    int rounds;
    int bettingAmount;
} Game_settings;

void initializeGameSettings(Game_settings game_setting_init);
Game *newGame(int id);
void startGame(Message msg, connection_t *connection, Game *game);
Game *addNewGame(connection_t *connection, Game *game);
Game *join_game(connection_t *connection, Game *game_selected);
Game *find_result(Game *game_selected);
int findWinner(Game *game);

#endif /* GAME_H */