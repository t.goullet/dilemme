
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/socket.h>
#include "controller.h"
#include "game.h"
#include "games.h"
#include "file.h"
#include "../common/variables.h"
#include "../common/communication.h"
#include "utils.h"
#include "server.h"

void controllerStopGame(Message msg, Game *game, int statusGame)
{
    if (statusGame == DISCONNECTED)
    {
        if (msg.player_id == game->player_1->index)
        {
            msg = resetMessage(msg);
            msg.status = GAMEEND;
            if (game->player_2 != NULL)
            {
                msg.player_id = game->player_2->index;
                msg.montant = game->p2Amount;
                sendMessage(msg, game->player_2);
            }
        }
        else
        {
            msg = resetMessage(msg);
            msg.montant = game->p1Amount;
            msg.player_id = game->player_1->index;
            msg.status = GAMEEND;
            sendMessage(msg, game->player_1);
        }
        removeGame(game->id);
    }
}

/**
 * @brief Update the amount of the player
 * @param msg
 * @param game
 **/
Message updateAmount(Game *game, Message msg, int player)
{
    if (player == P1)
    {
        msg.player_id = game->player_1->index;
        msg.montant = game->p1Amount;
    }
    else
    {
        msg.player_id = game->player_2->index;
        msg.montant = game->p2Amount;
    }

    if (game->round == game->maxRound)
    {
        msg.action = findWinner(game);
        msg.status = GAMEEND;
    }
    else
    {
        msg.action = game->result_round;
        msg.status = GAMESTART;
    }
    return msg;
}

/**
 * @brief Update game status and send message to users
 * @param msg
 * @param game
 **/
void updateGameUser(Message input_protocol, Game *game)
{
    if (game->status != GAMEWAITINGRESULTPLAYER)
    {
        game->status = GAMEWAITINGRESULTPLAYER;
    }
    else
    {
        game->round += 1;
        input_protocol.round = game->round;
        game = find_result(game);
        game->status = GAMERESULT;
        Message input_protocol1 = updateAmount(game, input_protocol, P1);
        Message input_protocol2 = updateAmount(game, input_protocol, P2);
        log_message(input_protocol1);
        log_message(input_protocol2);
        addResult(game);
        sendMessage(input_protocol1, game->player_1);
        sendMessage(input_protocol2, game->player_2);
    }
}

/**
 * @brief Define the choice for the right player
 * @param msg
 * @param game
 **/
void controller_choice(Message input_protocol, Game *game)
{
    // p1
    if (game->player_1->index == input_protocol.player_id)
    {
        game->p1Response = input_protocol.action;
        input_protocol.player_id = game->player_1->index;
        updateGameUser(input_protocol, game);
    }
    else
    { // p2
        game->p2Response = input_protocol.action;
        input_protocol.player_id = game->player_2->index;
        updateGameUser(input_protocol, game);
    }
}

/**
 * @brief Game controller deciding the action
 * @param msg
 * @param connection
 * @return 
 **/
void controllerMain(Message msg, connection_t *connection)
{

    Game *game;

    switch (msg.status)
    {
    //Disconnect players
    case DISCONNECTED:
        if (msg.game_id != 0)
        {
            game = searchById(msg.game_id);
            if (game != NULL)
            {
                controllerStopGame(msg, game, DISCONNECTED);
            }
        }
        break;
    case FIRSTPLAYERINGAME:
        game = searchValidGame();
        // If no game is free one is created
        if (game == NULL)
        {
            printf("A new player created the game\n");
            game = addNewGame(connection, game);
            msg.game_id = game->id;
            msg.status = WAITING;
            msg.round = game->round;
            log_message(msg);
            sendMessage(msg, connection);
        }
        else if (game->p2Response == WAITINGNEWPLAYER)
        {
            //P2 joining the game
            printf("A new player joined the game\n");
            game = join_game(connection, game);
            msg.game_id = game->id;
            msg.status = GAMESTART;
            msg.action = GAMEWAITINGRESULT;
            msg.round = game->round;

            startGame(msg, connection, game);
        }
        else
        {
            printf("Can not find game id : %d", game->id);
        }
        break;

    case GAMEWAITINGRESULTPLAYER:
        // Search game's player
        game = searchById(msg.game_id);

        if (game != NULL)
        {
            log_message(msg);
            controller_choice(msg, game);
        }

        break;
    case ERROR:
        printf("Error in game \n");
        break;
    default:
        printf("Error \n");
        break;
    }
}

/**
 * @brief display log for a message
 * @param msg
 * @return
 **/
void log_message(Message msg)
{
    printf("Protocol => Gameid : %d ; Status : %d ; IdPlayer : %d ; Action : %d ; Round : %d ; Montant : %d \n",
           msg.game_id, msg.status, msg.player_id,
           msg.action, msg.round, msg.montant);
}
