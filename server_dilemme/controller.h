
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../common/variables.h"
#include "../common/communication.h"
#include "server.h"
#include "game.h"

void controllerStopGame(Message msg, Game *game_selected, int statusGame);
Message updateAmount(Game *game, Message msg, int player);
void updateGameUser(Message input_protocol, Game *game);
void controller_choice(Message input_protocol, Game* game_selected);
void controllerMain(Message input_protocol, connection_t *connection);
#endif


