

#ifndef GAME_ARRAY_H
#define GAME_ARRAY_H

#include <stdbool.h>
#include "game.h"
#include "../common/variables.h"

typedef struct
{
    Game data[MAX_GAME];
    int index;
} Game_array;

void initGames();
void addGame(Game game_to_add);
bool removeGame(int index);
Game* searchValidGame();
Game* searchById(int id);

#endif