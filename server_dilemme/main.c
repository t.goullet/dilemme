/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: aurelio
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <stdbool.h>
#include "../common/config.h"
#include "../common/variables.h"
#include "server.h"
#include "games.h"
#include "game.h"
#include "file.h"
#include "utils.h"

Settings setting;
char *filename = "export.csv";

/**
 * @brief Function that initializes a Game_settings structure
 * @param setting
 * @return Game_settings
**/
Game_settings config_game(Settings setting)
{
    Game_settings game_setting;
    game_setting.rounds = setting.round;
    game_setting.bettingAmount = setting.bettingAmount;
    return game_setting;
}

int main(int argc, char **argv)
{
    int sockfd = -1;
    int index = 1;
    connection_t *connection;
    Game_array *game_array[MAX_GAME];

    pthread_t thread;
    /* init config file server*/
    settings_initialize_server(&setting, "../common/configServer.ini");

    /* init array*/
    init_sockets_array();
    /* init game config */
    displayInfoServer(setting.ip, setting.port);
    initializeGameSettings(config_game(setting));
    createFile(filename);
    /* create socket */
    sockfd = create_server_socket(setting);

    /* listen on port , stack size 50 for incoming connections*/
    if (listen(sockfd, 150) < 0)
    {
        fprintf(stderr, "%s: error: cannot listen on port\n", argv[0]);
        return -5;
    }

    //Wait for connection
    while (true)
    {
        /* accept incoming connections */
        connection = (connection_t *)malloc(sizeof(connection_t));
        connection->sockfd = accept(sockfd, &connection->address, &connection->addr_len);
        connection->index = index++;
        if (connection->sockfd <= 0)
        {
            free(connection);
        }
        else
        {
            /* start a new thread but do not wait for it */
            pthread_create(&thread, 0, threadProcess, (void *)connection);
            pthread_detach(thread);
        }
    }
    return (EXIT_SUCCESS);
}

/**
 * @brief Viewing Server Information in the Console (ip and port)
 * @param ip
 * @param port
 * @return 
 **/
void displayInfoServer(char *ip, char *port)
{
    printf("\n************************************\n");
    printf("Ready and Listening on %s:%s ", ip, port);
    printf("\n************************************\n");
}
