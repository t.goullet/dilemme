
DIRSERVER=./server_dilemme/
DIRCOMMON=./common/
DIRCONFIG=./config/
DIRCLIENT=./client_dilemme/

DOXYGENCONF=doxyfile
thread=-pthread
GTKthread=-pthread `pkg-config --cflags --libs gtk+-3.0` -rdynamic

CD=cd

server:
	${CD} $(DIRSERVER) && gcc -c *.c
	${CD} $(DIRCOMMON) && gcc -c *.c
	gcc ${thread} -o $(DIRSERVER)server $(DIRSERVER)*.o  $(DIRCOMMON)*.o

client:
	$(CD) $(DIRCLIENT) && gcc -c *.c ${GTKthread}
	${CD} $(DIRCOMMON) && gcc -c *.c
	gcc -o $(DIRCLIENT)client $(DIRCLIENT)*.o $(DIRCOMMON)*.o ${GTKthread}

runServer:
	${CD} $(DIRSERVER) && ./server

runClient:
	$(CD) $(DIRCLIENT) && ./client

doc:
	-doxygen $(DOXYGENCONF)



