# Dilemme du prisonnier

Personnes ayant partcipé au projet : 

    - Brette Nicolas 
    - Goullet Thibaut
    - Crozier Cyrian
    - Berthillon Mickael 

Le dépot Gitlab : 
[https://gitlab.com/NBrette/dilemme ](https://gitlab.com/NBrette/dilemme)

### Sommaire

- [Contexte](#Contexte)
- [Règles](#Règles)
- [Installation](#Installation)
- [Utilisation](#Utilisation)
- [Documentation](#Documentation)

## Contexte <a name="Contexte"></a>

L’ICS (institue of Cognitives Sciences) est un laboratoire interdisciplinaire qui intègre l'expertise de chercheurs des Sciences de la Vie (psychologie cognitive, neurosciences) et de médecine (pédopsychiatrie, neuro-pediatrie) avec celle de chercheurs des Sciences Humaines et Sociales (linguistique computationelle et théorique et philosophie) pour étudier la nature et la spécificité de l'esprit humain.

Le doctorant, qui n’est pas un développeur, a besoin d’accumuler des données expérimentales. Il a besoin que des volontaires jouent l’un contre l’autre un nombre de fois à définir, sans jamais savoir qui sont leurs adversaires. On définira une partie comme étant un certain nombre de rounds. Un round est défini comme une confrontation trahison-collaboration entre les deux volontaires.

## Règles <a name="Règles"></a>


Dans le dilemme du prisonnier deux joueurs s'affrontent dans différents rounds.
A chaque round les joueurs doivent prendre une décision et le résultat du round est défini de la manière suivante:  
- Les deux joueurs collaborent alors aucun ne perd d'argent
- Les deux joueurs trahissent alors ils perdent leur mise
- Le joueur 1 trahie et le joueur 2 collabore alors le joueur 1 double sa mise et le joueur 2 perd la sienne
- Le joueur 2 trahie et le joueur 1 collabore alors le joueur 2 double sa mise et le joueur 1 perd la sienne

## Installation <a name="Installation"></a>


La librairie GTK est nécessaire pour faire fonctionner l'interface graphique.
Pour les distributions basées sur debian:  
`sudo apt install libgtk-3-0`  
Pour Fedora:  
`dnf install gtk3`

Par la suite cloner les projet:  
```bash
git clone url
cd dilemme

#Compilation du client
make client
#Compilation du serveur
make server
```

## Utilisation <a name="Utilisation"></a>


Avant de lancer le programme il est nécessaire de le paramétrer. Le dossier common contient deux fichiers de configurations, un pour le client et l'autre pour le serveur.

Paramètres du serveur:
```ini
[server]
IP=127.0.0.1
port=7799
nbRounds=5
bettingAmount=10
```

Paramètres du client:
```ini
[client]
IP=127.0.0.1
port=7799
```

Une fois paramétré, le programme peut être lancé.
Pour démarrer le serveur:  
`make runServer`    
Pour démarrer le client:  
`make runClient`  

Une fois la partie terminée les résultats sont stockées dans un fichier `export.csv` à la racine du projet.


## Documentation <a name="Documentation"></a>
La commande suivante permet de générer la documentation:  
`make doc`  
Il suffit ensuite d'ouvrir le fichier `/html/index.html` pour accéder à la documentation.
