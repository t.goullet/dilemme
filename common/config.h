#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define SETTINGS_LINE_SIZE 300
#define SETTINGS_MAX_SETTINGS 10

typedef struct Settings{
    FILE* settings_file;
    char *filename;
    char *ip;
    char *port;
    int round;
    int bettingAmount;
} Settings;

void settings_initialize_server(Settings *setting, char *filename);
void settings_initialize_client(Settings *settings, char *file_name);
