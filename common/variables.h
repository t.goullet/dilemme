#ifndef VARIABLES_H
#define VARIABLES_H

// Protocol size
#define BUFFERSIZE 2048
// maximum client in server
#define MAXSIMULTANEOUSCLIENTS 100
#define GAME_CLIENT_WAITING_SERVER 0
#define MAX_GAME 50

//player choice during the round
#define COLLABORATE 11
#define BETRAY 12

#define STATUSMESSAGE 21
#define ACTIONMESSAGE 22

//round result 
#define BOTHCOLLAB 41
#define BOTHBETRAY 42
#define P1WIN 44
#define P2WIN 45
#define DRAW 43

// game status
#define ERROR 51
#define DISCONNECTED 52
#define WAITINGNEWPLAYER 53
#define FIRSTPLAYERINGAME 54
#define WAITING 55
#define READY 56
#define JOIN 57

#define P1 61
#define P2 62

#define GAMESTART 71
#define GAMERESULT 72
#define GAMEWAITINGRESULT 73
#define GAMEWAITINGRESULTPLAYER 74
#define GAMEEND 75

#endif