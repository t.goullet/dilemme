#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "communication.h"
#include "variables.h"

/**
 * @brief init message
 * @param message
 * @param id
 * @return return message initialized
 **/ 
Message newMessage(Message message, int id){
    message.game_id = 0;
    message.status = FIRSTPLAYERINGAME;
    message.player_id = id;
    message.action = 0;
    message.round = 0;
    message.montant = 100;
    return message;
}
/**
 * @brief Reset message values to 0
 * @param msg
 * @return the message reinitialized
 **/ 
Message resetMessage(Message msg){
    msg.game_id = 0;
    msg.status = 0;
    msg.player_id = 0;
    msg.action = 0;
    msg.round = 0;
    msg.montant = 0;
    return msg;
}

/**
 * @brief deserialize the string into a message
 * @param buffer
 * @return Message
 **/ 
Message deserialize(char *buffer){
    Message input_protocol;
      int input[10];

    char *val = strtok(buffer, ";");
    input[0] = atoi(val);
    for(int i = 1; i<= 4; i++){
        val = strtok(NULL, ";");
        if(val == NULL) 
            break;
        input[i] = atoi(val);
    }
    val = strtok(NULL, ";");
    input_protocol.montant = atoi(val);
    input_protocol.game_id = input[0];
    input_protocol.status = input[1];
    input_protocol.player_id = input[2];
    input_protocol.action = input[3];
    input_protocol.round = input[4];
    return input_protocol;
}
/**
 * @brief serialize the message into a string
 * @param message
 * @return message
 **/ 
char* serialize(Message message){
    char* output = malloc(256);
    sprintf(output, "%d;%d;%d;%d;%d;%d", 
    message.game_id,  message.status, message.player_id,
    message.action ,message.round, message.montant);
    return output;
}


