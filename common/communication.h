
#ifndef PROTOCOL_H
#define PROTOCOL_H

typedef struct {
    int game_id;
    int status;
    int player_id;
    int action;
    int round;
    int montant;
} Message;

Message newMessage(Message message, int id);
Message resetMessage(Message msg);
char* serialize(Message input_protocol);
Message deserialize(char *buffer_in);
#endif