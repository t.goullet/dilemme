#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "config.h"
#include "ini.h"

/**
 * @brief Initialize the settings for the server
 * @param settings
 * @param filename
 **/
void settings_initialize_server(Settings *settings, char *filename)
{
  ini_t *config_server = ini_load(filename);
  char *IP = ini_get(config_server, "server", "IP");
  char *port = ini_get(config_server, "server", "port");
  char *nbRounds = ini_get(config_server, "server", "nbRounds");
  char *bettingAmount = ini_get(config_server, "server", "bettingAmount");
  settings->ip = IP;
  settings->port = port;
  settings->bettingAmount = atoi(bettingAmount);
  settings->round = atoi(nbRounds);
}

/**
 * @brief Initialize the settings for the client
 * @param settings
 * @param filename
 **/
void settings_initialize_client(Settings *settings, char *filename)
{
  ini_t *config_server = ini_load(filename);
  char *IP = ini_get(config_server, "client", "IP");
  char *port = ini_get(config_server, "client", "port");

  settings->ip = IP;
  settings->port = port;
}
